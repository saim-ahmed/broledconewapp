/**
 *
 * HeaderBackButton
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import Colors from '../theme/Colors';
//import Dimensions from '../theme/Dimensions';
import elevation from '../theme/elevation';

// eslint-disable-next-line react/prefer-stateless-function
function HeaderBackButton(props) {
  const handleBackPress = () => {
    if (props.navigation) {
      props.navigation.goBack();
      return;
    }
    props.onPress();
  };

  if (!props.navigation && !props.onPress) {
    return null;
  }
  return (
    <TouchableOpacity
      style={[style.backButton, props.dark && style.backButtonDark]}
      onPress={() => handleBackPress()}
    >
      <Icon
        name="arrow-left"
        style={[style.backButtonIcon, props.dark && style.backButtonIconWhite]}
      />
    </TouchableOpacity>
  );
}

HeaderBackButton.propTypes = {
  navigation: PropTypes.object,
  onPress: PropTypes.func,
  dark: PropTypes.bool,
};

HeaderBackButton.defaultProps = {
  onPress: null,
  navigation: null,
  dark: false,
};


const style = StyleSheet.create({
  backButton: {
    width: 44,
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.primary1Color,
    borderRadius: 22,
    marginLeft: 2,
    ...elevation(1),
  },
  backButtonDark: {
    backgroundColor: Colors.transparent,
  },
  backButtonIcon: {
    fontSize: 24,
    color: Colors.textBlack,
  },
  backButtonIconWhite: {
    color: Colors.white,
  },
});


export default HeaderBackButton;
