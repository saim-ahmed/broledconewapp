import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import Text from './Text';
import BackButton from './BackButton';
import Colors from '../theme/Colors';
import Dimensions from '../theme/Dimensions';
import elevation from '../theme/elevation';

function Header({ navigation, title }) {
  return (
    <View style={style.container}>
      <BackButton navigation={navigation} />
      <Text style={style.title}>{title}</Text>
    </View>
  );
}

Header.propTypes = {
  navigation: PropTypes.func.isRequired,
  title: PropTypes.string,
};

Header.default = {
  title: '',
};

const style = StyleSheet.create({
  container: {
    padding: Dimensions.space1x,
    backgroundColor: Colors.primary1Color,
    flexDirection: 'row',
    ...elevation(2),
  },
  title: {
    fontSize: 24,
    fontWeight: '600',
    marginLeft: Dimensions.space1x,
    color: Colors.textBlack,
  },
});


export default Header;
