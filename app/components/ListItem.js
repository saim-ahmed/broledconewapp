import React from "react";
import { ScrollView , StyleSheet, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import * as Animatable from "react-native-animatable";
import Text from "./Text";
import Colors from "../theme/Colors";
import Dimensions from "../theme/Dimensions";
import elevation from "../theme/elevation";

function ListItem({ items, type, onPress}) {
  return (
    <ScrollView
      style={style.scrollView}
      contentContainerStyle={style.contentContainerStyle}
      bounces
      bouncesZoom
    >
      {Array.from({ length: items }, (item, index) => (
        <TouchableOpacity onPress={onPress} key={item}>
        <Animatable.View
          useNativeDriver
          animation="bounceIn"
          easing="ease-in-out"
          duration={100}
          style={style.container}
          key={item}
        >
        
          <Text style={style.label}>
            {index + 1} - Director {type}
          </Text>
          
        </Animatable.View>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
}
ListItem.propTypes = {
  navigation: PropTypes.object.isRequired,
  items: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};


const style = StyleSheet.create({
  scrollView: {
    height: Dimensions.screenHeight - 300,
    padding: Dimensions.space2x,
  },
  contentContainerStyle: {
    paddingBottom: Dimensions.space4x,
  },
  container: {
    justifyContent: "center",
    //   alignItems:'center',
    backgroundColor: Colors.white,
    borderRadius: Dimensions.radius3x,
    padding: Dimensions.space2x,
    marginVertical: Dimensions.space1x,
    ...elevation(1),
  },
  label: {
    fontSize: 20,
    fontWeight: "500",
    color: Colors.textBlack,
  },
});

export default React.memo(ListItem);
