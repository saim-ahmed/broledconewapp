import React from "react";
import { View } from "react-native";
import Text from "../Text";

import PropTypes from "prop-types";

import style from "./style";

function EarningDetail({ balance, TotalSignUps, possibleEarning }) {
  return (
    <View style={style.earningDetailContainer}>
      <View style={style.leftContainer}>
        <Text style={style.balanceLabel}>your balance</Text>
        <Text style={style.rsLabel}>RS: {balance}</Text>
      </View>
      <View style={style.rightContainer}>
        <View style={style.totalSignUpHolder}>
          <Text style={style.totalSignUpLabel}>Total SignUps:</Text>
          <Text style={style.noOfSignUps}>{TotalSignUps}</Text>
        </View>
        <View style={style.totalEarningHolder}>
          <Text style={style.totalEarningLabel}>Possible Earnings: </Text>
          <Text style={style.noOfPossibleEarnings}>Rs {possibleEarning}</Text>
        </View>
      </View>
    </View>
  );
}
EarningDetail.propTypes = {
  // navigation: PropTypes.func.isRequired,
  balance: PropTypes.number.isRequired,
  TotalSignUps: PropTypes.number.isRequired,
  possibleEarning: PropTypes.number.isRequired,
};

export default React.memo(EarningDetail);
