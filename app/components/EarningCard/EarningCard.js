import React, { useState } from "react";
import { View, TouchableOpacity } from "react-native";
import Text from "../Text";
import EarningDetail from "./earningDetail";

// import PropTypes from "prop-types";

import style from "./style";

function EarningCard() {
  const [activeTab, setActiveTab] = useState(0);
  return (
    <View style={style.container}>
      <Text style={style.mainHeading}>TOTAL EARNINGS</Text>
      <View style={style.earningContainer}>
        <View style={style.earningTabHolder}>
          <TouchableOpacity style={style.tab} onPress={() => setActiveTab(0)}>
            <Text
              style={[
                style.earnFromContactsLabel,
                activeTab === 0 ? style.activeTab : null,
              ]}
            >
              Earn from contacts
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.tab} onPress={() => setActiveTab(1)}>
            <Text
              style={[
                style.directReferralLabel,
                activeTab === 1 ? style.activeTab : null,
              ]}
            >
              Earn from direct referral
            </Text>
          </TouchableOpacity>
        </View>
        <EarningDetail balance={200} TotalSignUps={3} possibleEarning={400} />
      </View>
      <Text style={style.footerLabel}>Invites your contacts</Text>
    </View>
  );
}

export default React.memo(EarningCard);
