import { StyleSheet } from "react-native";
import Colors from "../../theme/Colors";
import Dimensions from "../../theme/Dimensions";
import elevation from "../../theme/elevation";

const style = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    padding: Dimensions.space3x,
    // paddingHorizontal: Dimensions.space3x,
    borderRadius: Dimensions.radius3x,
    marginTop: Dimensions.space3x,
    justifyContent: "space-between",
    ...elevation(3),
  },
  mainHeading: {
    fontSize: 20,
    fontWeight: "900",
    color: Colors.textBlack,
    alignSelf: "center",
  },
  earningContainer: {},
  earningTabHolder: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  tab: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
  },
  earnFromContactsLabel: {
    fontSize: 16,
    fontWeight: "500",
    color: Colors.corporate3Color,
  },
  directReferralLabel: {
    fontSize: 16,
    fontWeight: "500",
    color: Colors.corporate3Color,
    marginLeft: Dimensions.space3x,
  },
  activeTab: {
    borderBottomWidth: 3,
    borderColor: Colors.corporate1Color,
    paddingBottom: Dimensions.space2x,
  },

  earningDetailContainer: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderColor: Colors.primary2Color,
    paddingVertical: Dimensions.space2x,
  },
  leftContainer: {
    flex: 0.4,
    borderRightWidth: 1,
    paddingRight: Dimensions.space2x,
    borderRightColor: Colors.primary2Color,
  },
  balanceLabel: {
    fontSize: 14,
  },
  rsLabel: {
    fontSize: 14,
    fontWeight: "800",
    color: Colors.textBlack,
  },
  rightContainer: {
    flex: 0.6,
    paddingLeft: Dimensions.space1x,
  },
  totalSignUpHolder: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: Colors.primary2Color,
    justifyContent: "space-between",
    paddingBottom: Dimensions.space1x,
  },
  totalSignUpLabel: {
    fontSize: 14,
  },
  noOfSignUps: {
    fontSize: 14,
    fontWeight: "800",
    color: Colors.textBlack,
  },
  totalEarningHolder: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: Dimensions.space1x,
  },
  noOfPossibleEarnings: {
    fontSize: 14,
    fontWeight: "800",
    color: Colors.textBlack,
  },
  totalEarningLabel: {
    fontSize: 14,
  },
  footerLabel: {
    fontSize: 22,
    fontWeight: "bold",
    color: Colors.textBlack,
    alignSelf: "center",
    marginTop: Dimensions.space1x,
  },
});

export default style;
