import React, { Component } from 'react';
import { Text, View, ActivityIndicator } from 'react-native';

export class LoadingSpinner extends Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        if(this.props.visible){
            return (
                <View style={{flexDirection: 'row',alignSelf: 'center', alignItems: 'center', justifyContent: 'center', height: 60}}>
                    <ActivityIndicator size="large" color="#0000ff" />
                    <Text style={{color: '#666', fontSize: 14}}> Signing in </Text>
                </View>
            );
        } else {
            return <View />;
        }
    }
}


export default LoadingSpinner;
