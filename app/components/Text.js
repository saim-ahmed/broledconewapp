import React from "react";
import { Text, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import Colors from "../theme/Colors";


function TextComponent({ style: componentStyle, ...props }) {
  return <Text style={[style.defaultStyle, componentStyle]} {...props} />;
}
TextComponent.propTypes = {
  style: PropTypes.object,
};

TextComponent.defaultProps = {
  style: null,
};


const style = StyleSheet.create({
  defaultStyle: {
    // fontFamily: "Quicksand",
    backgroundColor: Colors.transparent,
  },
});

export default React.memo(TextComponent);
