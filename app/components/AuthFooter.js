import React, { useEffect } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { GoogleSignin } from 'react-native-google-signin';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import Colors from '../theme/Colors';
import Dimensions from '../theme/Dimensions';
const firebase = require('firebase');
require('firebase/auth');


function AuthFooter() {

  useEffect(
    () => {
      GoogleSignin.configure({
        webClientId:
          '905157181144-h8ouv2f4ci2bq7cccs4lfsf3mlhtd5jl.apps.googleusercontent.com',
        forceConsentPrompt: true // if you want to show the authorization prompt at each login
      });
    },
    []
  );
  const signInWithGoogle = () => {
    GoogleSignin.hasPlayServices()
    .then(() => {
        GoogleSignin.signIn()
        .then((data) => {
          // Create a new Firebase credential with the token
          const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
          // Login with the credential
          return firebase.auth().signInWithCredential(credential);
        })
        .then((user) => {
          // If you need to do anything with the user, do it here
          // The user will be logged in automatically by the
          // `onAuthStateChanged` listener we set up in App.js earlier
        })
        .catch((error) => {
          const { code, message } = error;
          // if (error.code === 'auth/account-exists-with-different-credential') {
          //   var pendingCred = error.credential;
          //   var email = error.email;
          //   auth.fetchSignInMethodsForEmail(email).then(function(methods) {
          //     if (methods[0] === 'password') {
          //       var password = promptUserForPassword(); //
          //       auth.signInWithEmailAndPassword(email, password).then(function(user) {
          //         // Step 4a.
          //         return user.linkWithCredential(pendingCred);
          //       }).then(function() {
          //         // Google account successfully linked to the existing Firebase user.
          //         goToApp();
          //       });
          //       return;
          //     }
          //   });
          // }
        });
    });
  }

  const signInWithFacebook = () => {
    LoginManager.logInWithPermissions(['public_profile', 'email'])
    .then((result) => {
      if (result.isCancelled) {
        return Promise.reject(new Error('The user cancelled the request'));
      }
      // Retrieve the access token
      return AccessToken.getCurrentAccessToken();
    })
    .then((data) => {
      // Create a new Firebase credential with the token
      const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
      // Login with the credential
      return firebase.auth().signInWithCredential(credential);
    })
    .then((user) => {
      // If you need to do anything with the user, do it here
      // The user will be logged in automatically by the
      // `onAuthStateChanged` listener we set up in App.js earlier
    })
    .catch((error) => {
      const { code, message } = error;
      // For details of error codes, see the docs
      // The message contains the default Firebase string
      // representation of the error
    });
    
  }
  
  return (
    <View style={style.container}>
      <TouchableOpacity 
        onPress={signInWithFacebook}
      >
      <View style={style.socialItem}>
        <Image style={style.facebookImage} 
               title='facebookIcon' 
               source={require('../images/facebookIcon.png')}
        />
        <Text style={style.text}>Facebook</Text>
      </View>
      </TouchableOpacity>
      <View style={style.separator} />
      <TouchableOpacity
        onPress={signInWithGoogle}
      >
      <View style={style.socialItem}>
        <Image style={style.googleImage} 
          title="googleIcon"
          source={require('../images/googleIcon.png')}
        />
        <Text style={style.text}>Google</Text>
      </View>
      </TouchableOpacity>
    </View>
  );
}

const style = StyleSheet.create({
    container: {
      justifyContent: 'space-around',
      alignItems: 'center',
      position: 'absolute',
      bottom: 0,
      width: Dimensions.screenWidth,
      backgroundColor: Colors.white,
      flexDirection: 'row',
    },
    separator: {
      borderWidth: 1,
      width: 1,
      height: 30,
      backgroundColor: Colors.primary1Color,
      opacity: 0.5,
    },
    socialItem: {
      borderTopWidth: 1,
      borderTopColor: Colors.primary1Color,
      backgroundColor: Colors.white,
      flexDirection: 'row',
      height: 50,
      justifyContent: 'center',
      alignItems: 'center',
    },
    facebookImage: {
      width: 30,
      height: 30,
      resizeMode: 'contain'
    },
    googleImage: {
      width: 35,
      height: 35,
      resizeMode: 'contain'
    },
    text: {
      fontSize: 16,
      fontWeight: 'bold',
      // fontFamily: 'Quicksand',
      color: Colors.textGrey,
      marginLeft: Dimensions.space2x,
    },
  });
  
export default React.memo(AuthFooter);