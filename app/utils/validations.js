export const isValidEmail = value =>
  /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value);

export const isValidName = name => {
  const re = /^[_A-z]*((-|\s)*[_A-z])*$/g;
  return re.test(String(name).toLowerCase());
};

export const isValidNumber = number =>
  /^([+]923|03)\d{2}-?\d{7}$/i.test(number);
