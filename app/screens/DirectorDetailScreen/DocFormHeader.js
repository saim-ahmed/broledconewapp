import React from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, StyleSheet } from "react-native";

import Text from "../../components/Text";
import Colors from '../../theme/Colors';
import Dimensions from '../../theme/Dimensions';
import elevation from '../../theme/elevation';


const style = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: Colors.primary2Color,
    height: 44,
  },
  headerHeading: {
    fontSize: 20,
    fontWeight: '500',
    color: Colors.textBlack,
    marginLeft: Dimensions.space2x,
  },
  closeIconHolder: {
    marginRight: Dimensions.space1x,
    height: 35,
    width: 35,
    justifyContent: 'center',
    alignItems: 'center',

    borderRadius: 22.5,
    borderWidth: 1,
    borderColor: Colors.primary2Color,
    // ...elevation(1),
  },
  closeIcon: {
    fontSize: 22,
    fontWeight: '500',
    color: Colors.textBlack,
  },
});

function Header({ onRequestClose, title }) {
  return (
    <View style={style.headerContainer}>
      <Text style={style.headerHeading}>{title}</Text>
      <TouchableOpacity style={style.closeIconHolder} onPress={onRequestClose}>
        <Text style={style.closeIcon}>X</Text>
      </TouchableOpacity>
    </View>
  );
}

Header.propTypes = {
  onRequestClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

//   Header.defaultProps = {
//     visible: false,
//   };

export default React.memo(Header);
