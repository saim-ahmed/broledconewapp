import React, { useEffect, useState } from "react";
import { View, StyleSheet, Alert } from "react-native";
import PropTypes from "prop-types";
import * as Animatable from "react-native-animatable";
import { Formik } from "formik";

import FlatButton from "../../theme/FlatButton";
import Input from "../../theme/Input";

import { isValidNumber, isValidEmail } from "../../utils/validations";

import Header from "./InfoFormHeader";

import Colors from "../../theme/Colors";
import Dimensions from "../../theme/Dimensions";
import elevation from "../../theme/elevation";

import firebaseConfig from '../../firebaseConfig';
const firebase = require('firebase');
// Required for side-effects
require('firebase/auth');
require('firebase/firestore');


const style = StyleSheet.create({
  modal: {
    flexDirection: "column",
    backgroundColor: Colors.white,
    width: Dimensions.screenWidth,
    paddingBottom: Dimensions.bottomSpacing,
    borderTopRightRadius: Dimensions.space4x,
    borderTopLeftRadius: Dimensions.space4x,
    ...elevation(1),
    position: "absolute",
    bottom: 0,
    left: 0,
    zIndex: 11,
  },
  loginButtonHolder: {
    padding: Dimensions.space3x,
  },
});

const initialValues = {
  fullName: "",
  phone: "",
  email: "",
  address: "",
};

const inputRefs = [];
const INPUT_FIELDS = [
  {
    key: "fullName",
    keyboardType: "default",
    placeholder: "Full Name",
    error: "invalid name",
  },
  {
    key: "phone",
    keyboardType: "numeric",
    placeholder: "Phone",
    error: "invalid name",
  },
  {
    key: "email",
    keyboardType: "email-address",
    placeholder: "Email",
    error: "invalid email",
  },
  {
    key: "address",
    keyboardType: "default",
    placeholder: "Address",
    error: "please fill address",
  },
];

function InfoForm({ visible, onRequestClose, directorNo }) {
  const [isVisible, setisVisible] = useState(visible);
  
  let db = null;
  useEffect(
    () => {
      let firebaseApp = null;
      // Initialize Firebase
      if (!firebase.apps.length) {
        firebaseApp = firebase.initializeApp(firebaseConfig);
      } else {
        firebase.apps.forEach((app) => {
          firebaseApp = app.name === '[DEFAULT]' ? app : null;
        });
      }
      db = firebaseApp ? firebaseApp.firestore() : null;

    }, []
  );

  const focusInput = index => {
    try {
      inputRefs[index].focus();
    } catch (e) {
      console.log(e);
    }
  };
  const validateFiled = values => {
    const errors = {};
    if (values.fullName === "") {
      errors.fullName = true;
    }
    if (values.phone === "" || !isValidNumber(values.phone)) {
      errors.phone = true;
    }
    if (values.email === "" || !isValidEmail(values.email)) {
      errors.email = true;
    }
    if (values.address === "") {
      errors.address = true;
    }
    return errors;
  };

  const renderField = (field, formikProps) => {
    const { key } = field;
    const value = formikProps.values[key];
    return (
      <View style={style.inputWrapper} key={key}>
        <Input
          innerRef={node => {
            inputRefs[field.index] = node;
          }}
          keyboardType={field.keyboardType}
          value={value}
          onChangeText={e => formikProps.setFieldValue(`${key}`, e)}
          placeholder={field.placeholder}
          onBlur={() => {
            formikProps.setFieldTouched(`${key}`);
          }}
          error={
            formikProps.touched[key] && formikProps.errors[key]
              ? field.error
              : null
          }
          isValid={!!(formikProps.touched[key] && !formikProps.errors[key])}
          onSubmitEditing={() => focusInput(field.index + 1)}
          {...field.inputProps}
        />
      </View>
    );
  };

  const onFormSubmit = (values) => {
    const { fullName, phone, email, address } = values;
    const director = { id: directorNo, fullName, phone, email, address };
    if(db){
      db.collection('directors').add(director);
      Alert.alert('Success', 'Director\'s information added successfully.', [{ text: 'OK', onPress: () => {setisVisible(false);} }]);
    }
  };

  return isVisible ? (
    <Animatable.View
      useNativeDriver
      animation="bounceIn"
      easing="ease-in-out"
      duration={200}
      style={style.modal}
    >
      <Header
        title="Fill Director Information"
        onRequestClose={onRequestClose}
      />
      <Formik
        key="form"
        initialValues={initialValues}
        onSubmit={onFormSubmit}
        validate={validateFiled}
        render={formikProps => (
          <React.Fragment>
            {INPUT_FIELDS.map((field, index) =>
              renderField({ ...field, index }, formikProps)
            )}
            <View style={style.loginButtonHolder}>
              <FlatButton
                onPress={formikProps.submitForm}
                label="submit"
                secondary
                round
                disable={
                  Object.keys(formikProps.errors).length !== 0 ||
                  Object.keys(formikProps.touched).length < 1
                }
              />
            </View>
          </React.Fragment>
        )}
      />
    </Animatable.View>
  ) : null;
}
InfoForm.propTypes = {
  //   navigation: PropTypes.func.isRequired,
  directorNo: PropTypes.number,
  visible: PropTypes.bool,
  onRequestClose: PropTypes.func.isRequired,
};

InfoForm.defaultProps = {
  visible: false,
};

export default React.memo(InfoForm);
