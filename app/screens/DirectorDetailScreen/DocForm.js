import React from 'react';
import { View, TouchableOpacity,StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import * as Animatable from 'react-native-animatable';
import { Formik } from 'formik';
import Icon from 'react-native-vector-icons/Feather';

import FlatButton from '../../theme/FlatButton';
import Text from '../../components/Text';

import Header from './DocFormHeader';
import Colors from '../../theme/Colors';
import Dimensions from '../../theme/Dimensions';
import elevation from '../../theme/elevation';

const style = StyleSheet.create({
  modal: {
    flexDirection: 'column',
    backgroundColor: Colors.white,
    width: Dimensions.screenWidth,
    paddingBottom: Dimensions.bottomSpacing,
    borderTopRightRadius: Dimensions.space4x,
    borderTopLeftRadius: Dimensions.space4x,
    ...elevation(1),
    position: 'absolute',
    bottom: 0,
    left: 0,
    zIndex: 11,
  },
  loginButtonHolder: {
    padding: Dimensions.space2x,
  },
  cardWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: Dimensions.space2x,
    marginVertical: Dimensions.space2x,
    marginHorizontal: Dimensions.space1x,
    borderRadius: Dimensions.radius3x,
    ...elevation(1),
  },
  cardTitle: {
    marginLeft: Dimensions.space1x,
    fontSize: 18,
    fontWeight: '700',
    color: Colors.textBlack,
  },
});


const initialValues = {
  doc1: '',
  doc2: '',
};

const INPUT_FIELDS = [
  {
    key: 'doc1',
    title: 'Upload passport copy or driving license or new NIC',
  },
  {
    key: 'doc2',
    title: 'Upload billing proof',
  },
];

function DocForm({ visible, onRequestClose }) {
  const renderField = field => {
    const { key } = field;
    return (
      <TouchableOpacity style={style.cardWrapper} key={key}>
        <Icon name='file-text' size={40} />
        <Text style={style.cardTitle}>{field.title}</Text>
      </TouchableOpacity>
    );
  };
  return visible ? (
    <Animatable.View
      useNativeDriver
      animation='bounceIn'
      easing='ease-in-out'
      duration={200}
      style={style.modal}
    >
      <Header title='Add Director Documents' onRequestClose={onRequestClose} />
      <Formik
        key='form'
        initialValues={initialValues}
        onSubmit={values => console.log('values', values)}
        validate={null}
        render={formikProps => (
          <React.Fragment>
            {INPUT_FIELDS.map((field, index) =>
              renderField({ ...field, index }, formikProps)
            )}
            <View style={style.loginButtonHolder}>
              <FlatButton
                onPress={() => console.log('Sign In')}
                label='submit'
                secondary
                round
                disable={
                  Object.keys(formikProps.errors).length !== 0 ||
                  Object.keys(formikProps.touched).length < 1
                }
              />
            </View>
          </React.Fragment>
        )}
      />
    </Animatable.View>
  ) : null;
}
DocForm.propTypes = {
  //   navigation: PropTypes.func.isRequired,
  visible: PropTypes.bool,
  onRequestClose: PropTypes.func.isRequired,
};

DocForm.defaultProps = {
  visible: false,
};

export default React.memo(DocForm);
