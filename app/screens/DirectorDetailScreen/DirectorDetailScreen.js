import React, { useState } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Header from '../../components/Header';
import ListItem from '../../components/ListItem';

import DirectorCountCard from './DirectorCountCard';
import InfoForm from './InfoForm';
import DocForm from './DocForm';
import Colors from '../../theme/Colors';
import Dimensions from '../../theme/Dimensions';
// import elevation from '../../theme/elevation';

function DirectorDetailScreen({ navigation }) {
  const [activeTab, setActiveTab] = useState(0);
  const [directorCount, setDirectorCount] = useState(1);
  const [directorNo, setDirectorNo] = useState(1);
  const [infoForm, setInfoForm] = useState(false);
  const [docForm, setDocForm] = useState(false);
  return (
    <View>
      <Header navigation={navigation} title="Director Detail" />
      <View style={[style.container]}>
        <View style={style.countCardHolder}>
          <DirectorCountCard
            directorCount={directorCount}
            onPressUp={() => setDirectorCount(directorCount + 1)}
            onPressDown={() => {
              if (directorCount === 1) {
                setDirectorCount(1);
                return;
              }
              setDirectorCount(directorCount - 1);
            }}
          />
          <View style={style.percentCard} />
        </View>
        <View style={style.tabHolder}>
          <TouchableOpacity style={style.tab} onPress={() => setActiveTab(0)}>
            <Text
              style={[
                style.informationLabel,
                activeTab === 0 ? style.activeTab : null,
              ]}
            >
              Fill Director Information
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.tab} onPress={() => setActiveTab(1)}>
            <Text
              style={[
                style.documentLabel,
                activeTab === 1 ? style.activeTab : null,
              ]}
            >
              Upload Director Documents
            </Text>
          </TouchableOpacity>
        </View>

        <ListItem
          type={activeTab === 0 ? 'Information' : 'Document'}
          items={directorCount}
          onPress={(index) => {
            setDirectorNo(index+1);
            if (activeTab === 0) {
              setInfoForm(true);
              return;
            }
            setDocForm(true);
          }}
          navigation={navigation}
        />
      </View>
      {infoForm || docForm ? <View style={style.overlay} /> : null}
      <InfoForm onRequestClose={() => setInfoForm(false)} visible={infoForm} directorNo={directorNo} />
      <DocForm onRequestClose={() => setDocForm(false)} visible={docForm} directorNo={directorNo} />
    </View>
  );
}
DirectorDetailScreen.propTypes = {
  navigation: PropTypes.func.isRequired,
};

const style = StyleSheet.create({
  container: {
    // padding: Dimensions.space2x,
  },
  countCardHolder: {
    flexDirection: 'row',
    padding: Dimensions.space2x,
    backgroundColor: Colors.corporate1Color,
  },
  percentCard: {
    width: 100,
    height: 50,
  },
  tabHolder: {
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: Colors.primary2Color,
    padding: Dimensions.space2x,
    marginVertical: Dimensions.space2x,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  tab: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  informationLabel: {
    fontSize: 16,
    fontWeight: '500',
    color: Colors.corporate3Color,
  },
  documentLabel: {
    fontSize: 16,
    fontWeight: '500',
    color: Colors.corporate3Color,
    marginLeft: Dimensions.space3x,
  },
  activeTab: {
    borderBottomWidth: 3,
    borderColor: Colors.corporate1Color,
    paddingBottom: Dimensions.space2x,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: Dimensions.screenHeight,
    width: Dimensions.screenWidth,
    backgroundColor: Colors.translucentBlack,
    opacity: 0.8,
    zIndex: 10,
  },
});

export default React.memo(DirectorDetailScreen);
