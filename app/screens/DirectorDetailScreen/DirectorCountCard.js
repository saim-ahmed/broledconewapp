import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';
import Text from '../../components/Text';
import Colors from '../../theme/Colors';
import Dimensions from '../../theme/Dimensions';
import elevation from '../../theme/elevation';

const style = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: Colors.primary1Color,
    borderRadius: Dimensions.radius3x,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: Dimensions.space1x,
    ...elevation(2),
  },
  heading: {
    fontSize: 20,
    fontWeight: '500',
    color: Colors.textBlack,
    paddingLeft: Dimensions.space1x,
  },
  countHolder: {
    // margin: Dimensions.space2x,
    marginLeft: Dimensions.space1x,
    paddingHorizontal: Dimensions.space1x,
    borderWidth: 1,
    borderColor: Colors.primary2Color,
    backgroundColor: Colors.primary1Color,
    justifyContent: 'center',
    alignItems: 'center',
  },
  countLabel: {
    fontSize: 20,
    fontWeight: '500',
    color: Colors.textBlack,
  },
});


function DirectorCountCard({
  directorCount,
  onPressUp,
  onPressDown,
  navigation,
}) {
  return (
    <View style={style.container}>
      <Text style={style.heading}>Number Of Directors</Text>
      <View style={style.countHolder}>
        <TouchableOpacity onPress={onPressUp}>
          <Icon name='chevron-up' size={30} color='black' />
        </TouchableOpacity>
        <Text style={style.countLabel}>{directorCount}</Text>
        <TouchableOpacity onPress={onPressDown}>
          <Icon name='chevron-down' size={30} color='black' />
        </TouchableOpacity>
      </View>
    </View>
  );
}
DirectorCountCard.propTypes = {
  navigation: PropTypes.func.isRequired,
  onPressUp: PropTypes.func.isRequired,
  onPressDown: PropTypes.func.isRequired,
  directorCount: PropTypes.number.isRequired,
};

export default React.memo(DirectorCountCard);
