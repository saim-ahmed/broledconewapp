import React from "react";
import { View, StyleSheet } from "react-native";
import PropTypes from "prop-types";

import ProgressCard from "./ProgressCard";
import Header from "../components/Header";
import Colors from "../theme/Colors";
import Dimensions from "../theme/Dimensions";

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    padding: Dimensions.space2x,
  },
});


function ProgressScreen({ navigation }) {
  return (
    <React.Fragment>
      <Header navigation={navigation} title="Progress" />
      <View style={style.container}>
        <ProgressCard title="Mobile Number Verification" />
        <ProgressCard title="Reservation of Company Name " />
        <ProgressCard title="Payment for application processing" />
        <ProgressCard title="Upload director document and detail" />
        <ProgressCard title="Upload share holders document and detail" />
        <ProgressCard title="Review of directors and share holders documents " />
      </View>
    </React.Fragment>
  );
}
ProgressScreen.propTypes = {
  navigation: PropTypes.func.isRequired,
};

export default React.memo(ProgressScreen);
