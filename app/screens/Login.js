import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, Image, StyleSheet, Alert, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import Header from '../components/Header';
import AuthFooter from '../components/AuthFooter';
import FlatButton from '../theme/FlatButton';
import LoadingSpinner from '../components/LoadingSpinner';
import Input from '../theme/Input';
import Checkbox from '../theme/CheckBox';
import Colors from '../theme/Colors';
import Dimensions from '../theme/Dimensions';

import firebaseConfig from '../firebaseConfig';
const firebase = require('firebase');
// Required for side-effects
require('firebase/firestore');
require('firebase/auth');


const initialValues = {
  userEmail: '',
  password: '',
};

const inputRefs = [];
const INPUT_FIELDS = [
  {
    key: 'userEmail',
    keyboardType: 'default',
    placeholder: 'Email',
    error: 'invalid email',
  },

  {
    key: 'userPassword',
    keyboardType: 'default',
    placeholder: 'Password',
    error: 'invalid password',
    inputProps: {
      secureTextEntry: true,
    },
  },
];


function LoginScreen(props) {
  const [rememberPass, setRememberPass] = useState(false);
  const [isLoading, setLoading] = useState(false);

  let db = null;

  useEffect(
    () => {
      let firebaseApp = null;
      // Initialize Firebase
      if (!firebase.apps.length) {
        firebaseApp = firebase.initializeApp(firebaseConfig);
      } else {
        firebase.apps.forEach((app) => {
          firebaseApp = app.name === '[DEFAULT]' ? app : null;
        });
      }
      db = firebaseApp ? firebaseApp.firestore() : null;

    }, []
  );
 
  useEffect(
    () => {
      const authSubscription = firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          let userInfo = {};
          if (user.providerId === 'google.com' || user.providerId === 'facebook.com') {
            user.providerData.forEach((profile) => {
              userInfo = {
                providerId: profile.providerId,
                UID: user.uid,
                name: profile.displayName,
                email: profile.email,
                photoURL: profile.photoURL,
              };
            });
          } else {
            userInfo = {
              UID: user.uid,
              email: user.email,
            };
          }
          db.collection('users').doc(user.uid).set(userInfo);
        }
      });
      return authSubscription;
    },
    []
  );
  
  const validateFiled = (values) => {
    const errors = {};
    if (values.userEmail === '') {
      errors.userEmail = true;
    }
    if (!values.userEmail.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
      errors.userEmail = true;
    }
    if (values.userPassword === '') {
      errors.password = true;
    }

    return errors;
  };

  const renderField = (field, formikProps) => {
    const { key } = field;
    const value = formikProps.values[key];
    return (
      <View style={style.inputWrapper} key={key}>
        <Input
          innerRef={(node) => {
            inputRefs[field.index] = node;
          }}
          keyboardType={field.keyboardType}
          value={value}
          onChangeText={e => formikProps.setFieldValue(`${key}`, e)}
          placeholder={field.placeholder}
          onBlur={() => {
            formikProps.setFieldTouched(`${key}`);
          }}
          error={
            formikProps.touched[key] && formikProps.errors[key]
              ? field.error
              : null
          }
          isValid={!!(formikProps.touched[key] && !formikProps.errors[key])}
          {...field.inputProps}
        />
      </View>
    );
  };


  const onFormSubmit = (values) => {
    setLoading(true);
    const { userEmail, userPassword } = values;
    firebase.auth().signInWithEmailAndPassword(userEmail, userPassword)
    .then((user) => {
      setLoading(false);
      // If you need to do anything with the user, do it here
      // The user will be logged in automatically by the 
      // `onAuthStateChanged` listener we set up in App.js earlier
    })
    .catch((error) => {
      setLoading(false);
      const { code, message } = error;
      switch (code) {
        case 'auth/network-request-failed':
          Alert.alert('ERROR', 'Internet connection lost! Try again.', [{ text: 'OK', onPress: () => {} }]);
          break;
        case 'auth/user-disabled':
          Alert.alert('ERROR', 'This user does not exist!', [{ text: 'OK', onPress: () => {} }]);
          break;
        case 'auth/account-exists-with-different-credential':
            var pendingCred = error.credential;
            var email = error.email;
            Alert.alert(
              'ALERT', 
              `You have previously signed in with a different method. 
              Do you want to proceed signin with email/password?`, 
              [
                 { text: 'No', onPress: () => {}, style: 'cancel' },
                 { text: 'Yes', onPress: () => {
                    firebase.auth().signInWithEmailAndPassword(email, userPassword).then(function(user) {
                      return user.linkWithCredential(pendingCred);
                    }).then(function() {
                      // Google account successfully linked to the existing Firebase user.
                    });
                 }}
              ]
            );
        break;
        default:
          Alert.alert('Error', 'Unknown error! Try again.', [{ text: 'OK', onPress: () => {} }]);
          break;
      }
    });
  };
  
  return (
    <React.Fragment>
      <Header navigation={props.navigation} />
      <View style={style.screen}>
        <ScrollView
          style={style.scrollView}
          contentContainerStyle={style.contentContainer}
          showsVerticalScrollIndicator={false}
        >
          <View style={style.headerContent}>
            <Image style={style.logo} source={require('../images/logo.png')} />
            <Text style={style.welcomeHeading}>Welcome</Text>
            <Text style={style.loginHeading}>Login</Text>
          </View>

          <Formik
            key='form'
            initialValues={initialValues}
            onSubmit={onFormSubmit}
            validate={validateFiled}
            render={formikProps => (
              <React.Fragment>
                {INPUT_FIELDS.map((field, index) =>
                  renderField({ ...field, index }, formikProps)
                )}
                <View style={style.userHelpHolder}>
                  <Checkbox
                    active={rememberPass}
                    onPress={() => setRememberPass(true)}
                    label='Remember password'
                  />
                </View>
                <LoadingSpinner visible={isLoading} />
                <View style={style.loginButtonHolder}>
                  <FlatButton
                    onPress={formikProps.submitForm}
                    label='Login'
                    secondary
                    round
                  />
                </View>
              </React.Fragment>
            )}
          />
           <TouchableOpacity>
             <View style={style.register}>
             <Text > Create new account </Text>
             </View>
           </TouchableOpacity>
        </ScrollView>
      </View>
      <AuthFooter />
    </React.Fragment>
  );
}


const style = StyleSheet.create({
  screen: {
    flex: 1,
    width: Dimensions.screenWidth,
    height: Dimensions.screenHeight,
    backgroundColor: Colors.white
  },
  headerContent: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollView: {
    marginBottom: 80
  },
  contentContainer: {
    paddingTop: Dimensions.space8x,
  },
  logo: {
    width: 150,
    height: 80,
    resizeMode: 'contain'
    // marginTop: Dimensions.space2x,
  },
  welcomeHeading: {
    fontSize: 20,
    // fontFamily: 'Quicksand',
    fontWeight: '500',
    color: Colors.textBlack,
    marginTop: Dimensions.space2x,
  },
  loginHeading: {
    fontWeight: 'bold',
    // fontFamily: 'Quicksand',
    color: Colors.corporate1Color,
    // marginTop: Dimensions.space2x,
  },
  inputHolder: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingVertical: Dimensions.space2x,
  },
  userHelpHolder: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: Dimensions.space2x,
    justifyContent: 'space-between',
  },
  forgetPassLabel: {
    fontSize: 14,
    color: Colors.textBlack,
    marginLeft: Dimensions.space1x,
  },
  loginButtonHolder: {
    padding: Dimensions.space3x,
  },
  facebook: {
    backgroundColor: Colors.facebook,
  },
  google: {
    backgroundColor: Colors.google,
  },
  register: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  }
});

LoginScreen.propTypes = {
  navigation: PropTypes.func.isRequired,
};

export default React.memo(LoginScreen);
