import React from "react";
import { View, Image,StyleSheet } from "react-native";
import PropTypes from "prop-types";
import ProgressCard from "./ProgressCard";
import DocumentCard from "../components/DocumentCard/DocumentCard";
import EarningCard from "../components/EarningCard/EarningCard";

import Colors from "../theme/Colors";
import Dimensions from "../theme/Dimensions";
import elevation from '../theme/elevation';

function HomeScreen({ navigation }) {
  return (
    <View style={style.container}>
            <Image style={style.logo} source={require('../images/logo.png')} />
      <ProgressCard steps={3} navigation={navigation} />
      <EarningCard />
      <DocumentCard navigation={navigation} />
    </View>
  );
}
HomeScreen.propTypes = {
  navigation: PropTypes.func.isRequired,
};


const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        padding: Dimensions.space2x,
      },
      logo: {
        width: Dimensions.screenWidth - 100,
        height: 80,
        resizeMode: 'contain',
        alignSelf: "center",
      },    
  });
  
export default React.memo(HomeScreen);
