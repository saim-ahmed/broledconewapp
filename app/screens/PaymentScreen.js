import React from "react";
import { View, ScrollView, Text } from "react-native";
import PropTypes from "prop-types";
import { Formik } from "formik";
import Icon from "react-native-vector-icons/Ionicons";
import Header from "../components/Header";
import Checkbox from "../theme/CheckBox";
import FlatButton from "../theme/FlatButton";

import CHECK_FIELDS from "./checkBoxData";
import { StyleSheet } from "react-native";
import Colors from "../theme/Colors";
import Dimensions from "../theme/Dimensions";
import elevation from "../theme/elevation";

const style = StyleSheet.create({
  scrollView: {
    height: 260,
    // borderWidth: 1,
    // borderColor: Colors.corporate1Color,
    paddingVertical: Dimensions.space2x,
    ...elevation(1),
  },
  checkBoxWrapper: {
    padding: Dimensions.space1x,
  },
  checkBoxHolder: {
    justifyContent: "center",
    padding: Dimensions.space1x,
    margin: Dimensions.space1x,
    borderRadius: Dimensions.radius2x,
    backgroundColor: Colors.primary1Color,
    ...elevation(2),
  },
  paymentMethodContainer: {
    margin: Dimensions.space2x,
    padding: Dimensions.space2x,
    borderRadius: Dimensions.radius2x,
    ...elevation(1),
  },
  paymentHeader: {
    paddingBottom: Dimensions.space1x,
    borderBottomWidth: 1,
    borderBottomColor: Colors.corporate1Color,
  },
  paymentMethodHeading: {
    fontSize: 18,
    fontWeight: "800",
    color: Colors.textBlack,
  },
  paymentMethodCheckBoxHolder: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.primary1Color,
  },
});


const PAYMENT_METHODS = [
  {
    key: "credit",
    label: "Credit Debit Card",
  },
  {
    key: "payPal",
    label: "PayPal",
  },
  {
    key: "cash",
    label: "Jez Cash / M cash",
  },
  {
    key: "bank",
    label: "Bank Payment",
  },
];

class PaymentScreen extends React.PureComponent {
  constructor() {
    super();
    this.initialValues = {};
    this.paymentMethodInitialValues = {};
    CHECK_FIELDS.forEach(field => {
      this.initialValues[field.key] = false;
    });
    PAYMENT_METHODS.forEach(field => {
      this.paymentMethodInitialValues[field.key] = false;
    });
  }

  renderField = (field, formikProps) => {
    const { key, label } = field;
    const value = formikProps.values[key];
    return (
      <View style={style.checkBoxHolder} key={key}>
        <Checkbox
          active={value}
          onPress={() => formikProps.setFieldValue(`${key}`, !value)}
          label={label}
        />
      </View>
    );
  };

  renderPaymentMethod = (field, formikProps) => {
    const { key, label } = field;
    const value = formikProps.values[key];
    return (
      <View style={style.paymentMethodCheckBoxHolder} key={key}>
        <Checkbox
          active={value}
          onPress={() => formikProps.setFieldValue(`${key}`, !value)}
          label={label}
        />
      </View>
    );
  };

  render() {
    return (
      <React.Fragment>
        <Header navigation={this.props.navigation} title="Order Review" />
        <Formik
          key="form"
          initialValues={this.initialValues}
          render={formikProps => (
            <View style={style.checkBoxWrapper}>
              <ScrollView style={style.scrollView} showsVerticalScrollIndicator>
                {CHECK_FIELDS.map(field =>
                  this.renderField({ ...field }, formikProps)
                )}
              </ScrollView>
            </View>
          )}
        />
        <View style={style.paymentMethodContainer}>
          <View style={style.paymentHeader}>
            <Icon name="md-cash" size={30} color="black" />
          </View>
          <Text style={style.paymentMethodHeading}>Other Payment Methods</Text>
          <Formik
            key="form"
            initialValues={this.paymentMethodInitialValues}
            render={formikProps => (
              <React.Fragment>
                {PAYMENT_METHODS.map(field =>
                  this.renderPaymentMethod({ ...field }, formikProps)
                )}
              </React.Fragment>
            )}
          />
        </View>
        <FlatButton
          onPress={() => console.log("Sign In")}
          label="Pay Now (US$27)"
          secondary
        />
      </React.Fragment>
    );
  }
}
PaymentScreen.propTypes = {
  navigation: PropTypes.func.isRequired,
};

export default React.memo(PaymentScreen);
