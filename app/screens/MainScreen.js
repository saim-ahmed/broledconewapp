import React, { useState } from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { TabView } from 'react-native-tab-view';
import HomeScreen from './HomeScreen';
import PaymentScreen from './PaymentScreen';
import ContactScreen from './ContactScreen';
import MoreTabScreen from './MoreTabScreen';
import TabBarButton from '../components/TabBarButton';
import Colors from '../theme/Colors';
import Dimensions from '../theme/Dimensions';
import elevation from '../theme/elevation';

const TAB_BAR_HEIGHT = 50;
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    width: Dimensions.screenWidth,
    height: Dimensions.screenHeight,
    top: 0,
    left: 0,
    position: 'absolute',
  },

  tabBar: {
    width: Dimensions.screenWidth,
    height: TAB_BAR_HEIGHT + Dimensions.space2x,
    flexDirection: 'row',
    overflow: 'visible',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: Dimensions.space2x,
    marginTop: -Dimensions.space2x,
    ...Platform.select({
      ios: {
        ...elevation(2),
        height: TAB_BAR_HEIGHT + Dimensions.space2x + Dimensions.bottomSpacing,
        paddingBottom: Dimensions.bottomSpacing,
      },
      android: {
        ...elevation(8, false),
      },
    }),
  },
  tabBarBackground: {
    width: Dimensions.screenWidth,
    height: TAB_BAR_HEIGHT,
    zIndex: 1,
    flexDirection: 'row',
    overflow: 'visible',
    backgroundColor: Colors.white,
    borderTopWidth: 1,
    borderTopColor: Colors.primary1Color,
    bottom: 0,
    left: 0,
    position: 'absolute',
    flex: 1,
    ...Platform.select({
      ios: {
        ...elevation(2),
        height: TAB_BAR_HEIGHT + Dimensions.bottomSpacing,
        paddingBottom: Dimensions.bottomSpacing,
      },
      android: {},
    }),
  },
});

function MainScreen({ navigation }) {
  const [index, setIndex] = useState(0);
  // eslint-disable-next-line
  const [routes, setRoutes] = useState([
    { key: 'Home', icon: 'home' },
    { key: 'Payment', icon: 'shopping-cart' },
    { key: 'Contact', icon: 'message-square' },
    { key: 'More', icon: 'more-horizontal' },
  ]);
  // eslint-disable-next-line
  const renderScene = ({ route }) => {
    switch (route.key) {
      case 'Home':
        return <HomeScreen navigation={navigation} />;
      case 'Payment':
        return <PaymentScreen navigation={navigation} />;
      case 'Contact':
        return <ContactScreen navigation={navigation} />;
      case 'More':
        return <MoreTabScreen navigation={navigation} />;
      default:
        return null;
    }
  };
  // eslint-disable-next-line
  const renderTabBar = ({ navigationState, jumpTo }) => (
    <View style={style.tabBar}>
      <View style={style.tabBarBackground} />
      {navigationState.routes.map((route, i) => (
        <TabBarButton
          route={route}
          key={route.key}
          onPress={() => {
            jumpTo(route.key);
          }}
          active={i === navigationState.index}
        />
      ))}
    </View>
  );

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      renderTabBar={renderTabBar}
      onIndexChange={i => setIndex(i)}
      tabBarPosition="bottom"
    />
  );
}
MainScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default React.memo(MainScreen);
