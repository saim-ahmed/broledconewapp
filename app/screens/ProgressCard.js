import React from "react";
import { View, Animated, TouchableOpacity } from "react-native";
// import Image from "components/Image";
import Text from "../components/Text";
import PropTypes from "prop-types";

import { StyleSheet } from "react-native";
import Colors from "../theme/Colors";
import Dimensions from "../theme/Dimensions";
import elevation from "../theme/elevation";

const style = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: Colors.corporate1Color,
    padding: Dimensions.space2x,
    borderRadius: Dimensions.radius2x,
    marginTop: Dimensions.space2x,
    ...elevation(1),
  },
  title: {
    flex: 0.8,
    fontSize: 18,
    fontWeight: "bold",
    color: Colors.white,
  },
  itemDetail: {
    height: 100,
    backgroundColor: Colors.white,
    borderRadius: Dimensions.radius2x,
    ...elevation(2),
  },
});


class ProgressCard extends React.PureComponent {
  state = {
    width: 0,
  };

  animated = new Animated.Value(0);

  componentDidMount() {
    Animated.timing(this.animated, {
      toValue: this.props.steps / 12,
      duration: 2000,
    }).start();
  }

  setWidthFromLayout = event => {
    if (this.state.width) {
      return;
    }
    this.setState({ width: event.nativeEvent.layout.width });
  };

  render() {
    return (
      <View style={style.CardHolder}>
        <Text style={style.mainHeading}>Your Account Progress</Text>
        <Text style={style.subHeading}>Overall Progress</Text>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("progress")}
          style={style.progressBarContainer}
        >
          <View style={style.progressHeadingHolder}>
            <Text style={style.percentText}>
              {Math.trunc((this.props.steps / 12) * 100)}%
            </Text>
            <Text style={style.stepsText}>
              {this.props.steps} of 12 step completed
            </Text>
          </View>
          <View
            style={style.progressBarHolder}
            onLayout={this.setWidthFromLayout}
          >
            <Animated.View
              style={[
                style.progress,
                {
                  width: this.animated.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, this.state.width],
                  }),
                },
              ]}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
ProgressCard.propTypes = {
  navigation: PropTypes.func.isRequired,
  steps: PropTypes.number.isRequired,
};

export default React.memo(ProgressCard);
