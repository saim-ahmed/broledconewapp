import logo from "./logo.png";
import logoThumb from "./logoThumb.png";
import facebookIcon from "./facebookIcon.png";
import googleIcon from "./googleIcon.png";
import logoInverse from "./logoInverse.png";

const images = {
  logo,
  logoThumb,
  facebookIcon,
  googleIcon,
  logoInverse,
};

export default images;
