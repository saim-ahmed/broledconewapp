/**
 *
 * Checkbox
 *
 */

import React, { useEffect } from "react";
import { Animated, View, TouchableOpacity, Text, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/Feather";

import Colors from "./Colors";
import Dimensions from "./Dimensions";

function Checkbox({ active, onPress, label }) {
  const animation = new Animated.Value(active ? 1 : 0);

  useEffect(() => {
    runAnimation(active);
  }, [active]);

  const runAnimation = isActive => {
    Animated.timing(animation, {
      toValue: isActive ? 1 : 0,
      duration: 400,
    }).start();
  };

  return (
    <TouchableOpacity onPress={onPress} style={style.container}>
      <Animated.View
        style={[
          style.square,
          {
            borderColor: animation.interpolate({
              inputRange: [0, 1],
              outputRange: [Colors.textBlack, Colors.corporate3Color],
            }),
          },
        ]}
      >
        <Animated.View
          style={[
            style.innerSquare,
            {
              transform: [
                {
                  scale: animation,
                },
              ],
              opacity: animation,
            },
          ]}
        >
          <Icon name="check" size={12} color="white" />
        </Animated.View>
      </Animated.View>
      {label && (
        <View style={style.labelContainer}>
          <Text style={[style.label, active && style.activeLabel]}>
            {label}
          </Text>
        </View>
      )}
    </TouchableOpacity>
  );
}

Checkbox.propTypes = {
  active: PropTypes.bool.isRequired,
  onPress: PropTypes.func,
  label: PropTypes.string.isRequired,
};

Checkbox.defaultProps = {
  onPress: () => null,
};



const style = StyleSheet.create({
  container: {
    flexDirection: "row",
    paddingVertical: Dimensions.space1x,
    paddingHorizontal: Dimensions.space2x,

    alignItems: "center",
    opacity: 1,
  },
  square: {
    width: 20,
    height: 20,
    borderRadius: Dimensions.radius2x,
    borderWidth: 1,
    borderColor: Colors.textBlack,
    alignItems: "center",
    justifyContent: "center",
  },
  innerSquare: {
    borderRadius: Dimensions.radius2x,
    width: 14,
    height: 14,
    backgroundColor: Colors.corporate1Color,
    alignItems: "center",
    justifyContent: "center",
  },
  label: {
    fontSize: 14,
    color: Colors.textBlack,
    marginLeft: Dimensions.space1x,
  },
  activeLabel: {
    color: Colors.corporate1Color,
  },
});

export default React.memo(Checkbox);
