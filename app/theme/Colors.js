export default {
  white: "#ffffff",
  black: "#000000",
  yellow: "#FBFB36",
  Gray: "#9E9E9E",
  lightGray: "#f6f5f6",

  primary1Color: "#F0F0F0",
  primary2Color: "#DEDEDE",
  primary3Color: "#141412",

  corporate1Color: "#DF1E26",
  corporate2Color: "#D31624",
  corporate3Color: "#D32D39",

  textBlack: "#111",
  textGrey: "#555",

  transparent: "rgba(255, 255, 255, 0)",
  transparentBlack: "rgba(0, 0, 0, 0)",
  translucentBlack: 'rgba(0, 0, 0, 0.94)',

  inputBackground: "#EFEFEF",
  inputBorder: "#FFF",
  placeholderColor: "#AAA",

  errorBackground: "#DF1E26",
  errorForeground: "#FDFBFB",

  successBackground: "#006400",
  successForeground: "#FDFBFB",

  facebook: "#3C66C4",
  google: "#4285F4",
};
