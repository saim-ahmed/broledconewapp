/**
 *
 * FlatButton
 *
 */

import React from "react";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/Feather";
import { TouchableOpacity, Text, StyleSheet } from "react-native";
import Colors from "./Colors";
import Dimensions from "./Dimensions";
import elevation from "./elevation";


function FlatButton(props) {
  return (
    <TouchableOpacity
      style={[
        style.button,
        props.round ? style.roundButton : null,
        props.flex ? style.flex : {},
        props.secondary ? style.secondaryButton : {},
        props.active ? style.activeButton : {},
        props.disabled ? style.disabled : style.enabled,
        props.elevated ? style.elevated : null,
      ]}
      onPress={props.disabled ? () => null : props.onPress}
    >
      {props.iconName ? (
        <Icon
          style={[
            style.icon,
            props.secondary ? style.secondaryIcon : {},
            props.active ? style.activeIcon : {},
          ]}
          name={props.iconName}
        />
      ) : null}

      <Text
        style={[
          style.label,
          props.secondary ? style.secondaryLabel : {},
          props.active ? style.activeLabel : {},
        ]}
      >
        {props.label}
      </Text>
    </TouchableOpacity>
  );
}

FlatButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  iconName: PropTypes.string,
  secondary: PropTypes.bool,
  disabled: PropTypes.bool,
  flex: PropTypes.bool,
  elevated: PropTypes.bool,
  active: PropTypes.bool,
  round: PropTypes.bool,
};

FlatButton.defaultProps = {
  secondary: false,
  disabled: false,
  elevated: true,
  iconName: "",
  flex: false,
  active: false,
};


const style = StyleSheet.create({
  button: {
    backgroundColor: Colors.white,
    borderRadius: Dimensions.radius3x,
    margin: Dimensions.space1x,
    padding: Dimensions.space2x,
    paddingVertical: Dimensions.space1x,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    height: 44,
    ...elevation(1),
  },
  roundButton: {
    borderRadius: 20,
  },
  secondaryButton: {
    backgroundColor: Colors.corporate1Color,
  },
  activeButton: {
    borderWidth: 1,
    borderColor: Colors.corporate1Color,
  },
  flex: {
    flex: 1,
  },
  label: {
    fontSize: 18,
    color: Colors.primary3Color,
    maxWidth: Dimensions.screenWidth - Dimensions.space6x - 40,
    fontWeight: "800",
  },
  secondaryLabel: {
    color: Colors.primary1Color,
  },
  activeLabel: {
    color: Colors.corporate1Color,
  },
  disabled: {
    opacity: 0.5,
    ...elevation(0),
  },
  enabled: {
    opacity: 1,
  },
  elevated: {
    ...elevation(2),
  },
  icon: {
    fontSize: 22,
    marginRight: Dimensions.space2x,
    color: Colors.black,
  },
  secondaryIcon: {
    color: Colors.primary1Color,
  },
  activeIcon: {
    color: Colors.corporate1Color,
  },
});

export default FlatButton;
