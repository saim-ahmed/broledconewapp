import React from "react";
import { View, Text, TextInput, Animated, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/Feather";
import Colors from "./Colors";
import Dimensions from "./Dimensions";
import elevation from "./elevation";

class Input extends React.PureComponent {
  state = {
    focused: false,
  };

  animation = new Animated.Value(this.props.isValid ? 1 : 0);

  componentWillReceiveProps(nextProps) {
    this.runAnimation(nextProps.isValid);
  }

  runAnimation(isValid) {
    Animated.timing(this.animation, {
      toValue: isValid ? 1 : 0,
      duration: 400,
    }).start();
  }

  defaultSettings = {
    multiline: false,
    clearButtonMode: "unless-editing",
  };

  focus() {
    if (!this.input) {
      return;
    }
    this.input.focus();
  }

  render() {
    const { label, multiline, onBlur, onFocus, error, innerRef } = this.props;
    return (
      <View style={style.container}>
        {label ? <Text style={style.label}>{label}</Text> : null}
        <View
          style={[
            style.textHolder,
            multiline ? style.multilineContainer : style.singlelineContainer,
          ]}
        >
          <TextInput
            ref={node => {
              this.input = node;
              innerRef(node);
            }}
            underlineColorAndroid={Colors.transparent}
            placeholderTextColor={Colors.placeholderColor}
            onFocus={() => {
              this.setState({ focused: true });
              onFocus();
            }}
            onBlur={() => {
              this.setState({ focused: false });
              onBlur();
            }}
            style={[
              this.state.focused ? style.focusStyle : style.blurStyle,
              style.input,
              label ? style.labeledInput : null,
              multiline ? style.multiline : {},
            ]}
            {...this.defaultSettings}
            {...this.props}
          />
          <Animated.View
            style={[
              style.validIcon,
              {
                opacity: this.animation,
              },
            ]}
          >
            <Icon name="check" size={18} color={Colors.corporate1Color} />
          </Animated.View>
        </View>

        {error ? (
          <View style={style.error}>
            <Text style={style.errorLabel}>{error}</Text>
          </View>
        ) : null}
      </View>
    );
  }
}

Input.propTypes = {
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  placeholder: PropTypes.any,
  error: PropTypes.func,
  label: PropTypes.string,
  multiline: PropTypes.bool,
  isValid: PropTypes.bool,
  innerRef: PropTypes.func,
};

Input.defaultProps = {
  onFocus: () => null,
  onBlur: () => null,
  innerRef: () => null,
  placeholder: null,
  error: null,
  multiline: false,
  isValid: false,
};

const INPUT_HEIGHT = 30;
const MULTILINE_HEIGHT = 130;

const style = StyleSheet.create({
  container: {
    marginVertical: Dimensions.space2x,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    padding: Dimensions.space1x,
  },
  textHolder: {
    // position: "relative",
    // borderWidth: 1,
    // borderColor: Colors.primary1Color,
    flexDirection: "row",
    height: INPUT_HEIGHT,
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  multilineContainer: {
    height: MULTILINE_HEIGHT,
  },
  input: {
    minHeight: INPUT_HEIGHT,
    // width: Dimensions.screenWidth - 20,
    flex: 1,
    marginHorizontal: Dimensions.space2x,
    padding: Dimensions.space1x,
    // paddingHorizontal: Dimensions.space2x,
    borderRadius: Dimensions.radius2x,
    borderBottomWidth: 2,
    color: Colors.primary3Color,
    backgroundColor: Colors.white,
    alignSelf: "center",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    textAlignVertical: "center",
    position: "relative",
    // fontFamily: "Quicksand",
    borderBottomColor: Colors.corporate1Color,
  },
  labeledInput: {
    minHeight: INPUT_HEIGHT + 10,
    borderWidth: 1,
    ...elevation(1),
  },
  label: {
    marginBottom: Dimensions.space1x,
    marginHorizontal: Dimensions.space2x,
    fontWeight: "500",
    fontSize: 16,
  },
  blurStyle: {
    borderColor: Colors.primary2Color,
  },
  focusStyle: {
    borderColor: Colors.corporate1Color,
  },
  multiline: {
    height: MULTILINE_HEIGHT,
    padding: Dimensions.space2x,
    textAlignVertical: "top",
  },
  placeholderContainer: {
    position: "absolute",
    left: Dimensions.space3x,
    height: INPUT_HEIGHT,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  placeholder: {
    color: Colors.textGrey,
    fontSize: 16,
    width: Dimensions.screenWidth - Dimensions.space12x,
  },
  labeledPlaceHolder: {
    color: Colors.primary2Color,
  },
  loaderContainer: {
    position: "absolute",
    right: Dimensions.space1x,
    height: 44,
    top: 0,
    alignItems: "center",
    justifyContent: "center",
  },
  error: {
    backgroundColor: Colors.corporate1Color,
    paddingHorizontal: Dimensions.space2x,
    paddingVertical: Dimensions.space1x / 3,
    borderRadius: Dimensions.radius4x,
    // color: Colors.errorForeground,
    position: "absolute",
    bottom: -Dimensions.space3x,
    right: Dimensions.space4x,
    // fontWeight: "600",
    // fontSize: 13,
    overflow: "hidden",
  },
  // error: {
  //   marginHorizontal: Dimensions.space2x + 2,
  //   marginTop: -Dimensions.space1x,
  //   borderRadius: Dimensions.radius1x,
  //   backgroundColor: Colors.corporate1Color
  // },
  errorLabel: {
    color: Colors.white,
    fontWeight: "600",
    fontSize: 13,
  },
  validIcon: {
    position: "absolute",
    right: Dimensions.space2x,
  },
});

export default React.memo(Input);
