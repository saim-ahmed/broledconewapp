import { createStackNavigator, createAppContainer } from 'react-navigation';

import LoginScreen from './screens/Login';
import SignUpScreen from './screens/SignUpScreen';
import MainScreen from './screens/MainScreen';
import HomeScreen from './screens/HomeScreen';
import PaymentScreen from './screens/PaymentScreen';
import ContactScreen from './screens/ContactScreen';
import MoreTabScreen from './screens/MoreTabScreen';
import ProgressScreen from './screens/ProgressScreen';
import DirectorDetailScreen from './screens/DirectorDetailScreen/DirectorDetailScreen';

const Navigator = createStackNavigator(
  {
    login: {
      screen: LoginScreen,
    },
    signUp: {
      screen: SignUpScreen,
    },
    main: {
      screen: MainScreen,
    },
    home: {
      screen: HomeScreen,
    },
    payment: {
      screen: PaymentScreen,
    },
    contact: {
      screen: ContactScreen,
    },
    more: {
      screen: MoreTabScreen,
    },
    progress: {
      screen: ProgressScreen,
    },
    directorDetail: {
      screen: DirectorDetailScreen,
    },
  },
  {
    initialRouteName: 'directorDetail',
    defaultNavigationOptions: {
      headerStyle: {
        display: 'none',
      },
    },
  }
);

export default createAppContainer(Navigator);
